FROM maven:3-jdk-8-alpine

ENV TZ=Asia/Seoul

WORKDIR /usr/src/app

COPY ./alert-fep-boot/target/alert-fep-boot-1.0-spring-boot.jar /usr/src/app

EXPOSE 8085

CMD java -jar alert-fep-boot-1.0-spring-boot.jar
