package kr.hbh.alert.domain.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Data
public class ServiceResponse extends Response{

	List<String> userIds;
	List<Long> chatIds;

	public boolean isSuccess() {
		return status == result.SUCCESS ? true : false;
	}

	public void addUserId(String userId){
		if(userIds == null)
			userIds = new ArrayList<>();

		userIds.add(userId);
	}

	public void addChatId(long chatId){
		if(chatIds == null)
			chatIds = new ArrayList<>();

		chatIds.add(chatId);
	}

	public List<Map<String, Object>> getListMap1() {
		return (List<Map<String, Object>>) data;
	}

	public Response toResponse(){
		Response res = new Response();
		res.setStatus(this.status);
		res.setData(this.data);
		res.setErrMsg(this.errMsg);

		return res;
	}
}