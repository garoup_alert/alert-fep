package kr.hbh.alert.domain.prod;

public interface AlertProducer {
    void publish(Object payload);
}
