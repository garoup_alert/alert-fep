package kr.hbh.alert.domain.entity;

import lombok.Data;

import java.util.List;

@Data
public class AlertMsg {

	private List<String> target;
	private String severity;
	private String message;

	public String getMessage(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(this.severity);
		sb.append("] ");
		sb.append(this.message);

		return sb.toString();
	}

}
