package kr.hbh.alert.domain.entity;

import lombok.Data;

import java.util.List;

@Data
public class AlertMsgResult {

	public AlertMsgResult(int userCount){
		this.userCount = userCount;
	}

	private int userCount;

}
