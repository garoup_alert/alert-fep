package kr.hbh.alert.domain.service;

import kr.hbh.alert.domain.entity.AlertMsg;
import kr.hbh.alert.domain.entity.ServiceResponse;

public interface AlertFepService {

    ServiceResponse alertRequest(AlertMsg alertMsg);

}