package kr.hbh.alert.domain.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Data
public class Response {

	enum result { SUCCESS, FAIL }

	public void Success(Object data){
		status = result.SUCCESS;
		this.data = data;
	}

	public void Fail(String errMsg){
		status = result.FAIL;
		this.errMsg = errMsg;
	}

	protected result status;

	protected String errMsg;

	protected Object data;

}
