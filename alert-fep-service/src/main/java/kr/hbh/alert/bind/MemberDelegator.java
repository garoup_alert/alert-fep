package kr.hbh.alert.bind;

import kr.hbh.alert.domain.entity.ServiceResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Slf4j
public class MemberDelegator {

    //Member Service의 응답 메시지에서 중복을 제외한 사용자ID 조회
    public static ServiceResponse callMemberApi(String apiUrl){
        ServiceResponse res;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ServiceResponse> result = restTemplate.getForEntity(apiUrl, ServiceResponse.class);

        //요청 상태 정상
        if(result.getStatusCodeValue() == 200){
            res = result.getBody();
            if(res.isSuccess()){
                for(Map<String, Object> map : res.getListMap1()){
                    if(map.containsKey("chatId")){
                        long chatId = (int) map.get("chatId");
                        res.addChatId(chatId);
                    } else {
                        String userId = (String) map.get("userId");
                        res.addUserId(userId);
                    }
                }
            }

        } else {
            res = new ServiceResponse();
            res.Fail("HTTP Error! [" + result.getStatusCodeValue() + "]");
        }
        log.info("Call Service Status: " + result.getStatusCodeValue() + ", URL: " + apiUrl);
        return res;
    }

}
