package kr.hbh.alert.rest;

import kr.hbh.alert.domain.entity.ServiceResponse;
import kr.hbh.alert.domain.service.AlertFepService;
import lombok.extern.slf4j.Slf4j;

import kr.hbh.alert.domain.entity.AlertMsg;
import kr.hbh.alert.domain.entity.Response;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/v1.0/fep")
@Slf4j
public class AlertFepResource {

    private final AlertFepService alertFepService;

    public AlertFepResource(AlertFepService alertFepService){
        this.alertFepService = alertFepService;
    }

    @PostMapping
    public Response receiveAlert(@RequestBody AlertMsg alertMsg) {
        log.info("IF-FEP-001 Start!");
        Response res = new Response();

        try{
            //Validation
            if(alertMsg.getTarget() == null || alertMsg.getTarget().size() == 0){
                res.Fail("[target] cannot not be null!");
                return res;
            }
            if(alertMsg.getSeverity() == null || "".equals(alertMsg.getSeverity())){
                res.Fail("[severity] cannot not be null!");
                return res;
            }
            if(alertMsg.getMessage() == null || "".equals(alertMsg.getMessage())){
                res.Fail("[message] cannot not be null!");
                return res;
            }

            //msg send request
            ServiceResponse response = alertFepService.alertRequest(alertMsg);
            res = response.toResponse();

        } catch (Exception e){
            log.error("IF-FEP-001 Error!", e);
            res.Fail(e.getMessage());
        }

        return res;
    }

}