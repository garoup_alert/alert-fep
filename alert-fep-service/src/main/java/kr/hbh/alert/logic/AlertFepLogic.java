package kr.hbh.alert.logic;

import kr.hbh.alert.bind.MemberDelegator;
import kr.hbh.alert.domain.entity.AlertMsg;
import kr.hbh.alert.domain.entity.AlertMsgResult;
import kr.hbh.alert.domain.entity.Payload;
import kr.hbh.alert.domain.entity.ServiceResponse;
import kr.hbh.alert.domain.prod.AlertProducer;
import kr.hbh.alert.domain.service.AlertFepService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AlertFepLogic implements AlertFepService {

    @Value("${member.api.user:http://localhost:8084/api/v1.0/member/user}")
    String userApiUrl;

    @Value("${member.api.group:http://localhost:8084/api/v1.0/member/grouping}")
    String groupApiUrl;

    private final AlertProducer alertProducer;

    public AlertFepLogic(AlertProducer alertProducer){
        this.alertProducer = alertProducer;
    }

    @Override
    public ServiceResponse alertRequest(AlertMsg alertMsg) {
        ServiceResponse response = new ServiceResponse();
        String message = alertMsg.getMessage();

        //타겟 중 전체 처리
        if(alertMsg.getTarget().contains("@all")){
            log.info("Target is [@all]");
            //모든 사용자 조회
            response = MemberDelegator.callMemberApi(userApiUrl);

            //조회된 목록이 성공이면 Queue로 전달
            if(response.isSuccess()){
                if(response.getChatIds() != null){
                    response = enqueue(response.getChatIds(), message);
                } else {
                    log.warn("Target not exist!");
                    response.Success(new AlertMsgResult(0));
                }
            }

        //타겟 중 그룹, 사용자 처리
        } else {
            //alert 대상 사용자 목록
            List<String> userList = new ArrayList<>();

            //제공받은 타겟에서 그룹명 추출
            StringBuilder sbGroup = new StringBuilder();
            for(String target : alertMsg.getTarget()) {
                if("".equals(target)) { continue; }
                String prefixGroup = target.substring(0, 2);
                String prefixName = target.substring(0, 1);

                //타겟 중 그룹 추출
                if ("@@".equals(prefixGroup)) {
                    String groupId = target.substring(2);
                    sbGroup.append(groupId).append(",");
                }
                //타겟 중 사용자 추출
                else if ("@".equals(prefixName)) {
                    String userId = target.substring(1);
                    deduplicationUser(userId, userList);
                }
            }

            //그룹 기준 저장된 사용자 조회
            if(sbGroup.length() > 0){
                String gParam = sbGroup.toString().substring(0, (sbGroup.toString().length() - 1));
                log.info("Target is [@@group]: " + gParam);
                String apiUrl = groupApiUrl + "/" + gParam;
                log.info("groupApiUrl: " + groupApiUrl);
                response = MemberDelegator.callMemberApi(apiUrl);

                if(response.isSuccess()){
                    //사용자ID 중복제거
                    if(response.getChatIds() != null){
                        for(String userId : response.getUserIds()){
                            deduplicationUser(userId, userList);
                        }
                    } else {
                        log.warn("Target not exist!");
                        response.Success(new AlertMsgResult(0));
                    }

                } else {
                    return response;
                }
            }

            //저장된 사용자의 chatId 조회
            if(userList.size() > 0){
                log.info("Target is [@user]: " + userList);
                String userStr = userList.toString().replaceAll(" ", "").substring(1);
                String apiUrl = userApiUrl + "/" + userStr.substring(0, (userStr.length() - 1));

                response = MemberDelegator.callMemberApi(apiUrl);

                //조회된 목록이 성공이면 Queue로 전달
                if(response.getChatIds() != null){
                    if(response.isSuccess()){
                        response = enqueue(response.getChatIds(), message);
                    } else {
                        return response;
                    }
                } else {
                    log.warn("Target not exist!");
                    response.Success(new AlertMsgResult(0));
                }

            } else {
                log.warn("Target not exist!");
                response.Success(new AlertMsgResult(0));
            }
        }

        return response;
    }

    //중복 사용자 제외
    private void deduplicationUser(String userId, List<String> userList){
        if(!userList.contains(userId)){
            userList.add(userId);
        }
    }

    //중복 사용자 제외
    private ServiceResponse enqueue(List<Long> chatIds, String text){
        ServiceResponse res = new ServiceResponse();
        int userCount = chatIds.size();
        try{
            log.info("Enqueue Start! UserCont="+ userCount);
            for(long chatId : chatIds){
                alertProducer.publish(new Payload(chatId, text));
            }

            res.Success(new AlertMsgResult(userCount));
        } catch (Exception e) {
            log.error("Request Enqueue error!", e);
            res.Fail(e.getMessage());
        }

        return res;
    }
}

