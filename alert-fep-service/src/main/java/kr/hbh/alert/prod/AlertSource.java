package kr.hbh.alert.prod;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface AlertSource {
    String ALERT_OUTPUT = "alertOutput";

    @Output(AlertSource.ALERT_OUTPUT)
    MessageChannel output();

}
