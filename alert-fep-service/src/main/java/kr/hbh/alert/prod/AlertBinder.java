package kr.hbh.alert.prod;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import kr.hbh.alert.domain.prod.AlertProducer;

@Slf4j
@Component
@Data
@EnableBinding(AlertSource.class)
public class AlertBinder implements AlertProducer {

    private final AlertSource alertSource;

    public AlertBinder(AlertSource alertSource){
        this.alertSource = alertSource;
    }

    @Override
    public void publish(Object payload) {
        log.info("ALERT_CHANNEL publish Start!");
        log.debug("payload=" + payload);
        alertSource.output().send(MessageBuilder.withPayload(payload).build());
    }
}
